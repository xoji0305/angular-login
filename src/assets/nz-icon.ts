import {IconDefinition} from "@ant-design/icons-angular";
import {
  SettingOutline,
  EditOutline,
  EllipsisOutline,
  EnvironmentOutline
} from '@ant-design/icons-angular/icons';
export const NZ_ICONS_DEFINITION: IconDefinition[] = [
  SettingOutline,
  EditOutline,
  EllipsisOutline,
  EnvironmentOutline
]
