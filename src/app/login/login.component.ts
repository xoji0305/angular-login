import { Component, OnInit } from '@angular/core';
import {UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {NzMessageService} from "ng-zorro-antd/message";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
   providers: [NzMessageService]
})
export class LoginComponent implements OnInit {

  public loginForm!: UntypedFormGroup;

  constructor(private formBuilder: UntypedFormBuilder,
              private http: HttpClient,
              private readonly messageService: NzMessageService,
              private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      lastname:[null, [Validators.required]],
      password:[null, [Validators.required]]
    })
  }

  login() {
    this.http.get<any>('http://localhost:3000/signupUsers')
      .subscribe(res => {
        const user = res.find((a: any) => {
          return a.lastname === this.loginForm.value.lastname && a.password === this.loginForm.value.password
        });
        if (user) {
          this.loginForm.reset();
          this.router.navigate(['home']).then();
        } else {
          this.messageService.error("Bunday foydalanuvchi topilmadi :(");
        }
      }, error => {
        this.messageService.warning("Nimadir xato ketti!!" + error);
      });
    if (!this.loginForm.value?.lastname || !this.loginForm.value?.password) {
      if(this.loginForm.invalid) this.messageService.error("Foydalanuvchi nomi va parolni kiritish majbury");
      return;
    }
  }
}
