import {Component} from "@angular/core";
import {UserAddress} from "../home/user.model";

@Component({
  selector: 'map',
  templateUrl: 'map.component.html',
  styles: [
    `
      .map-container {
        width: 100%;
        height: 400px;
      }
    `
  ]
})

export class MapComponent {
  address: UserAddress;
  placemarkProperties: ymaps.IPlacemarkProperties = {
    hintContent: 'Hint content',
    balloonContent: 'Baloon content',
  };

  placemarkOptions: ymaps.IPlacemarkOptions = {
    iconLayout: 'default#image',
    iconImageHref:
      'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png',
    iconImageSize: [32, 32],
  };
}
