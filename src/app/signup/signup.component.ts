import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public signupForm!: UntypedFormGroup;
  constructor( private formBuilder: UntypedFormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.signupForm = this.formBuilder.group({
      fullname: [''],
      lastname: [''],
      email: [''],
      password: [''],
      mobile: ['']
    });
  }

  signUp() {
    this.http.post<any>("http://localhost:3000/signupUsers", this.signupForm.value)
      .subscribe(res => {
        this.http.get('http://localhost:3000/signupUsers')
          .subscribe((res: any) => {
            const user = res.find((a: any) => {
              return a.lastname === this.signupForm.value.lastname && a.password === this.signupForm.value.password
            });
            if (user) {
              alert("Ro'yhatdan o'tish muvaffaqiyatli bajarildi");
              this.signupForm.reset();
              this.router.navigate(['home']).then();
            }
          });
      }, error => {
        alert('Nimadir xato ketti!!');
      })
  }

}
