import {Injectable} from "@angular/core";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {User} from "./user.model";

@Injectable({
  providedIn: 'root'
})

export class UserService {
  readonly resourceUrl = 'http://localhost:3000/users';
  public updateData = new Subject<object>();
  constructor(private readonly http: HttpClient) {
  }

  query():Observable<HttpResponse<User[]>> {
    return this.http.get<User[]>(this.resourceUrl, {observe: 'response'});
  }

  update(user: User): Observable<HttpResponse<User>> {
    return this.http.put<User>(`${this.resourceUrl}/${user.id}`, user, {observe: 'response'});
  }

  create(user: User): Observable<HttpResponse<User>> {
    return this.http.post<User>(this.resourceUrl, user, {observe: 'response'});
  }

  delete(id: number): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/${id}`, {observe: 'body'});
  }

  updateUserData(data: any):void {
    this.updateData.next(data);
  }

}
