import {Component, OnInit} from "@angular/core";
import {UserService} from "./user.service";
import {User, UserAddress} from "./user.model";
import {NzModalService} from "ng-zorro-antd/modal";
import {MapComponent} from "../map/map.component";
import {UserModalComponent} from "./user-update/user-modal.component";
import {NzDrawerService} from "ng-zorro-antd/drawer";
import {race} from "rxjs";

@Component({
  selector: 'Home',
  templateUrl: 'home.component.html',
  providers: [NzModalService]
})

export class HomeComponent implements OnInit{
  users: User[] = [];
  constructor(private readonly userService: UserService,
              private readonly modalService: NzModalService,
              private readonly drawerService: NzDrawerService
  ) {}

  ngOnInit():void {
    this.userService.query()
      .subscribe((res: any) => {
        this.users = res.body;
      });
    this.userService.updateData
      .subscribe((data: any) => {
        this.users.forEach((item: User) => {
          if (data.body.id === item.id) {
            const index = this.users.indexOf(item);
            this.users[index] = data.body;
          }
        });
        if (data.status === 201) {
          this.users.push(data.body);
        }
      })
  }

  openDrawer(user?: User):void {
    this.drawerService.create({
      nzContent: UserModalComponent,
      nzContentParams: {user},
      nzWidth: 500
    })
  }

  openMap(address: UserAddress):void {
    this.modalService.create({
      nzContent: MapComponent,
      nzFooter: null,
      nzComponentParams: {address}
    })
  }

  delete(user: User):void  {
    this.userService.delete(user.id)
      .subscribe((data:any) => {
        if (data) {
          const index = this.users.indexOf(user);
          if (index > -1)this.users.splice(index, 1);
        }
      })
  }
}
