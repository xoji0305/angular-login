import {Component, OnInit} from "@angular/core";
import {User} from "../user.model";
import {UntypedFormBuilder} from "@angular/forms";
import {UserService} from "../user.service";
import {NzDrawerService} from "ng-zorro-antd/drawer";

@Component({
  templateUrl: 'user-modal.component.html'
})

export class UserModalComponent implements OnInit{
  user: User;
  userForm = this.fb.group({
    id: [null],
    name: [null],
    username: [null],
    email: [null],
    address: this.fb.group({
      street: [null],
      suite: [null],
      city: [null],
      zipcode: [null],
      geo: this.fb.group({
        lat: [null],
        lng: [null]
      })
    }),
    phone: [null],
    website: [null],
    company: this.fb.group({
      name: [null],
      catchPhrase: [null],
      bs: [null]
    })
  });
  constructor(private readonly fb: UntypedFormBuilder,
              private readonly userService: UserService,
              private readonly drawerService: NzDrawerService
  ) {}

  ngOnInit():void {
    if (this.user) {
      this.userForm.patchValue({...this.user})
    }
  }

  submit():void {
    if (this.user) {
      this.userService.update(this.userForm.value)
        .subscribe((data:any) => {
          this.userService.updateUserData(data);
        })
    } else {
      this.userService.create(this.userForm.value)
        .subscribe((data: any) => {
          this.userService.updateUserData(data);
        })
    }
  }
}
