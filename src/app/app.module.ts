import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import {RouterModule, Routes} from "@angular/router";
import { NavbarComponent } from './navbar/navbar.component';
import {HomeComponent} from "./home/home.component";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzCardModule} from "ng-zorro-antd/card";
import {NzAvatarModule} from "ng-zorro-antd/avatar";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NZ_ICONS_DEFINITION} from "../assets/nz-icon";
import {NzListModule} from "ng-zorro-antd/list";
import {MapComponent} from "./map/map.component";
import {AngularYandexMapsModule, YaConfig} from "angular8-yandex-maps";
import {UserModalComponent} from "./home/user-update/user-modal.component";
import {NzEmptyModule} from "ng-zorro-antd/empty";
import {AboutComponent} from "./about/about.component";
import {NzDrawerModule} from "ng-zorro-antd/drawer";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputNumberModule} from "ng-zorro-antd/input-number";
import {NzInputModule} from "ng-zorro-antd/input";

const mapConfig: YaConfig = {
  apikey: 'API_KEY',
  lang: 'en_US',
};

const routes: Routes = [
  {
    path: '', redirectTo: 'login', pathMatch: 'full'
  },
  {
    path: 'home', component: HomeComponent
  },
  {
    path: 'about', component: AboutComponent
  },
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'signup', component: SignupComponent
  },
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    NavbarComponent,
    HomeComponent,
    MapComponent,
    UserModalComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NzButtonModule,
    NzCardModule,
    NzAvatarModule,
    NzIconModule.forRoot(NZ_ICONS_DEFINITION),
    NzListModule,
    AngularYandexMapsModule.forRoot(mapConfig),
    NzEmptyModule,
    NzDrawerModule,
    NzFormModule,
    NzInputNumberModule,
    NzInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
